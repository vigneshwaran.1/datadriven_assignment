package register_DemoWeb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Register {
	
	public static void main(String[] args) throws IOException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		File fs = new File("/home/vigneshwaran/SeleniumPractice/DataDriven_DemoWeb/src/test/resources/Data/Demo_Web_Datadriven.xlsx");
		FileInputStream input = new FileInputStream(fs);
		XSSFWorkbook book = new XSSFWorkbook(input);

		XSSFSheet sheetAt = book.getSheetAt(0);
		int physicalNumberOfRows = sheetAt.getPhysicalNumberOfRows();
		String user = sheetAt.getRow(0).getCell(0).getStringCellValue();
		String lastn = sheetAt.getRow(0).getCell(1).getStringCellValue();
		String mail = sheetAt.getRow(0).getCell(2).getStringCellValue();
		String pass = sheetAt.getRow(0).getCell(3).getStringCellValue();
		String pass1 = sheetAt.getRow(0).getCell(4).getStringCellValue();
		driver.findElement(By.partialLinkText("Register")).click();
		driver.findElement(By.id("FirstName")).sendKeys(user);
		driver.findElement(By.id("LastName")).sendKeys(lastn);
		driver.findElement(By.id("Email")).sendKeys(mail);
		driver.findElement(By.name("Password")).sendKeys(pass);
		driver.findElement(By.name("ConfirmPassword")).sendKeys(pass1);
		driver.findElement(By.id("register-button")).click();

	}


}
